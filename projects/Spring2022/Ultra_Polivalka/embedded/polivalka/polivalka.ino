#include <Servo.h> 

#define SERVO_PIN 11
#define MOISTURE_PIN A0

long water_duration = 2000;
long cycle_duration = 60000; // 1 min
long timer_duration = 600000; // 10 min
int moisture_border = 500;
unsigned long waterMillis = millis();
unsigned long paramsMillis = millis(); 
unsigned long timerMillis = millis(); 

Servo servo1;

char current_mode = none_mode;

void setup() {
  Serial.begin(9600);
  pinMode(MOISTURE_PIN, INPUT);
  pinMode(SERVO_PIN, OUTPUT);
  servo1.attach(SERVO_PIN);
  close_water();
}

void loop() {
  if (millis() - waterMillis >= cycle_duration) {
    waterMillis = millis();
    mode_cycle();
  }
  params_cycle();
}
