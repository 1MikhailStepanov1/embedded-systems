void water(int milliseconds) {
  open_water();
  delay(milliseconds);
  close_water();
}

void open_water() {
  servo1.write(90);
}

void close_water() {
  servo1.write(0);
}

int read_moisture() {
  return analogRead(MOISTURE_PIN);
}
